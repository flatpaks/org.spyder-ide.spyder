# Spyder IDE flatpak

A flatpak for the [Spyder IDE](https://www.spyder-ide.org/).

**This project is a work in progress, don't expect anything for now.**

## Usage

Once installed, type `flatpak run org.spyder-ide.spyder` to run te Spyder IDE.

## Reporting

Issues concerning the flatpak should be reported on the issue tracker linked to this repository.

Issues concerning the Spyder IDE should be reported [here](https://github.com/spyder-ide/spyder).

## License

Both the Spyder IDE and this flatpak are published under [MIT license](./LICENSE).
